FROM python:3.11-slim as builder

WORKDIR /code

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
    apt-get install -y --no-install-recommends gcc
    
COPY . /code/

RUN pip install -r requirements.txt

VOLUME ["/code/db"]

EXPOSE 8000

CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000